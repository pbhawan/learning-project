﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningProject.Automation.Utilities
{
    public enum Browsers
    {
        Firefox,
        RemoteFireFox,
        RemoteChrome,
        InternetExplorer,
        Chrome
    }
}
