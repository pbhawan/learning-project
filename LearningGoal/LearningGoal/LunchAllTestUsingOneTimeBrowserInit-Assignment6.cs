﻿
using NUnit.Framework;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace LearningGoal
{


    [TestFixture]
    public class LunchAllTestUsingOneTimeBrowserInit_Assignment6
    {
        IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
        [OneTimeSetUp]
        public void setup()
        {

            ChormeDriver.Navigate().GoToUrl("http://automationpractice.com/index.php");
        }
        [OneTimeTearDown]
        public void Teardown()
        {
            System.Console.WriteLine("end browser");
            this.ChormeDriver.Quit();

        }

        [Test]
        public void VerifyHomePageIsRenderdAfterClickingOnYourLogo_SingleBrowserInit()
        {

            ChormeDriver.FindElement(By.XPath("//*[@id='block_top_menu']/ul/li[2]/a")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='header_logo']/a/img")).Click();
            string URL = ChormeDriver.Url;
            Assert.AreEqual(URL, "http://automationpractice.com/index.php", "Home Page  not Launched");


        }
        [TestCase(Category = "ABC")]
        public void VerifyContactUsPage_SingleBrowserInit()
        {
           
            ChormeDriver.FindElement(By.XPath("//*[@id='contact-link']/a")).Click();
            String ContactUs_Text = ChormeDriver.FindElement(By.XPath("//*[@id='center_column']/h1")).Text;
            Assert.AreEqual(ContactUs_Text, "CUSTOMER SERVICE - CONTACT US", "Contact Us Page not renderd");

        }
        [Test]
        public void VerifyNoResultForSerachKeyword_SingleBrowserInit()
        {
         
            ChormeDriver.FindElement(By.XPath("//*[@id='search_query_top']")).SendKeys("ABCDEFGH");
            ChormeDriver.FindElement(By.XPath("//*[@id='searchbox']/button")).Click();
            String Search_Result = ChormeDriver.FindElement(By.XPath("//*[@id='center_column']/p")).Text;
            Assert.AreEqual(Search_Result, "No results were found for your search \"ABCDEFGH\"", " Result found");

        }
    }
}
