﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LearningGoal
{
    [TestFixture]
    public class Locators_XPath__Assignment4
    {
        [Test]
        public void CountTotalInputBoxesAvailableOnTheForm()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            ReadOnlyCollection<IWebElement> allText = ChormeDriver.FindElements(By.XPath("//input[@type='text']"));
            int i = allText.Count;
            Console.Out.Write("Input Boxes Available = "+i);

        }

        [Test]
        public void ColorCodeoftheBoxWithTitleChangeColor()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            IWebElement ColorCode = ChormeDriver.FindElement(By.XPath("//button[@id='colorVar']"));
            Console.Out.Write("Color of Change Color Text = " + ColorCode.GetCssValue("color"));
        }
        [Test]
        public void CountTotalNumberofButtonAvailableOnTheForm()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            ReadOnlyCollection<IWebElement> allText = ChormeDriver.FindElements(By.XPath("//button"));
            int i = allText.Count;            
            Console.Out.Write("Number of Buttons Available on form = "+i);         

        }
        [Test]
        public void WriteTextof6thAndLastButton()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            ReadOnlyCollection<IWebElement> allText = ChormeDriver.FindElements(By.XPath("//button"));
            int i = allText.Count;
            string sixthElement = allText[5].Text;
            string LastElement = allText[8].Text;
            Console.Out.Write("6th Element = " +sixthElement +", 8th Element = "+ LastElement);
         
        }
        [Test]
        public void SaveAllLinksAndGetTextAssociatedLinkNode()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            ReadOnlyCollection<IWebElement> allText = ChormeDriver.FindElements(By.XPath("//a[@herf]"));
            int i = allText.Count;
            string sixthElement = allText[5].Text;
            string LastElement = allText[8].Text;
            Console.Out.Write(sixthElement + LastElement);
            Console.Write(sixthElement + LastElement);
            Console.WriteLine("Text you wish to be outputted to the Console");
            Console.ReadKey();

        }
    }
    
}
