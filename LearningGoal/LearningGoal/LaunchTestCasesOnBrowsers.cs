﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using LearningProject.Automation.Utilities;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;



namespace LearningProject.Automation.LaunchTestCasesOnBrowsers
{
    
      public class LaunchTestCasesOnBrowsers
    {
        IWebDriver ChormeDriver;
        IWebDriver FirfoxFireDriver;
        IWebDriver IEDriver;

        public void Initialize()
        {
            //  ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            // FirfoxFireDriver = new FirefoxDriver();
            //IEDriver=new InternetExplorerDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");

        }
        [Test]
        public void LaunchTestCaseOnChrome()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("https://www.facebook.com/login/");
        }
        [Test]
        public void LaunchTestCaseOnFirefox()
        {
            FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
            IWebDriver FirfoxFireDriver = new FirefoxDriver(service);
            FirfoxFireDriver.Navigate().GoToUrl("https://bing.com");
        }
        [Test]
        public void LaunchTestCaseOnIE()
        {
            IWebDriver IEDriver = new InternetExplorerDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            IEDriver.Navigate().GoToUrl("https://bing.com");
            //driver.get
        }
             
    }
}
