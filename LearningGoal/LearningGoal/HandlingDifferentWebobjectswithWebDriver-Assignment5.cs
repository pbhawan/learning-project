﻿
using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LearningGoal
{
    [TestFixture]
    public class HandlingDifferentWebobjectswithWebDriver_Assignment5
    {
        [Test]
        public void ClickOnLinkElement8()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            System.Threading.Thread.Sleep(45000);
            IWebElement Element8 = ChormeDriver.FindElement(By.XPath("//a[contains(text(),'Element8')]"));
            Element8.Click();    
           

        }
        [Test]
        public void LaunchApplicatonAndFillFields()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://toolsqa.wpengine.com/automation-practice-form/");
            ChormeDriver.FindElement(By.Name("firstname")).SendKeys("Pratibha");
            ChormeDriver.FindElement(By.Name("lastname")).SendKeys("Bhawan");
             ChormeDriver.FindElement(By.XPath("//input[@id='sex-1']")).Click();
            ChormeDriver.FindElement(By.XPath("//input[@id='exp-5']")).Click();
            ChormeDriver.FindElement(By.XPath("//input[@id='datepicker']")).SendKeys("16/12/2018");
            ChormeDriver.FindElement(By.XPath("//input[@id='profession-1']")).Click();
            ChormeDriver.FindElement(By.XPath("//input[@id='tool-2']")).Click();
            ChormeDriver.FindElement(By.Id("continents")).FindElement(By.XPath(".//option[contains(text(),'South America')]")).Click();
            ChormeDriver.FindElement(By.Id("selenium_commands")).FindElement(By.XPath(".//option[contains(text(),'Switch Commands')]")).Click();
            ChormeDriver.FindElement(By.XPath("//Button[@id='submit']")).Click();
        }
        [Test]
        public void Top10Gainer()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://money.rediff.com/losers/bse/daily");
            ChormeDriver.FindElement(By.XPath("//a[contains(@href, 'money.rediff.com/gainers/bse')]")).Click();
            int iColumnCount = ChormeDriver.FindElements(By.XPath("//*[@id='leftcontainer']/table/thead/tr/th")).Count;
            ReadOnlyCollection<IWebElement> Rows = ChormeDriver.FindElements(By.XPath("//*[@id='leftcontainer']/table/tbody/tr/td[1]"));
           
                for (int i = 0; i <= 10; i++)
            {
                string s = Rows[i].Text;
                Console.Out.WriteLine("Top 10 gainer = " + s);

            }

        }
        [Test]
        public void Top10Loser()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://money.rediff.com/losers/bse/daily");
            ChormeDriver.FindElement(By.XPath("//a[contains(@href, 'money.rediff.com/losers/bse/weekly')]")).Click();
            int iColumnCount = ChormeDriver.FindElements(By.XPath("//*[@id='leftcontainer']/table/thead/tr/th")).Count;
            ReadOnlyCollection<IWebElement> Rows = ChormeDriver.FindElements(By.XPath("//*[@id='leftcontainer']/table/tbody/tr/td[1]"));

            for (int i = 0; i <= 10; i++)
            {
                string s = Rows[i].Text;
                Console.Out.WriteLine("Top 10 gainer = " + s);

            }

        }
    }
}
