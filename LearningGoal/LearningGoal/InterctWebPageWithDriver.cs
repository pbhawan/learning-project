﻿using System;

using LearningProject.Automation.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using NUnit.Framework;

namespace LearningGoal
{
    [TestFixture]
    public class InterctWebPageWithDriver
    {
        
        [Test]
        public void NavigateToBackFromHomePage()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            ChormeDriver.Navigate().Back();
        }

        [Test]
        public void DisplayTitleOfTheLink()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            IWebElement Link = ChormeDriver.FindElement(By.ClassName("wpb_wrapper"));
            string s = Link.Text;         
        }
        [Test]
        public void EnterValidEmailAndClickSubscribeButton()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            IWebElement EmailText = ChormeDriver.FindElement(By.XPath("//input[@name='email']"));
            EmailText.SendKeys("pratibhab0490@gmail.com");
            IWebElement Subscribebutton = ChormeDriver.FindElement(By.XPath("//input[@value = 'Subscribe']"));
            Subscribebutton.Click();         
        }

        [Test]
        public void FilltheContactUSForm()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://www.seleniumframework.com/Practiceform/");
            IWebElement Name = ChormeDriver.FindElement(By.Name("name"));
            Name.SendKeys("Pratibha");
            IWebElement Email = ChormeDriver.FindElement(By.Name("email"));
            Email.SendKeys("pratibhab0490@gmail.com");
            IWebElement Phone = ChormeDriver.FindElement(By.Name("telephone"));
            Phone.SendKeys("9414518217");
            IWebElement country = ChormeDriver.FindElement(By.XPath("//input[@name='country']"));
            country.SendKeys("India");
            IWebElement company = ChormeDriver.FindElement(By.XPath("//input[@name='company']"));
            company.SendKeys("Metacube");
            IWebElement message = ChormeDriver.FindElement(By.XPath("//textarea[@name='message']"));
            message.SendKeys("Message for you");
            IWebElement Submit = ChormeDriver.FindElement(By.XPath("//a[@class='dt-btn dt-btn-m dt-btn-submit']"));
            Submit.Click();

        }
    }
}
