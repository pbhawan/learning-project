﻿
using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LearningGoal
{

    public class Assertion_Assignment6
    {

        [Test]
        public void AssertionFor3and5areNotEqual()
        {
            try
            {
                Assert.AreNotEqual(3, 5, "Both are equal");
                Console.Out.Write("Both are not equal");
            }
            catch (Exception e)
            {
                Console.Out.Write(e);

            }


        }
        [Test]
        public void AssertionFor3isGreaterThen5()
        {
            try
            {
                Assert.Greater(3, 5, "3 is not greater then 5");
                Console.Out.Write("3 is not greater then 5");
            }
            catch (Exception e)
            {
                Console.Out.Write(e);

            }
        }


        [Test]
        public void Palindrome()
        {
            int n = 16461, r, sum = 0, temp;
            temp = n;
            while (n > 0)
            {
                r = n % 10;
                sum = (sum * 10) + r;
                n = n / 10;
            }

            try
            {
                Assert.AreEqual(temp, sum, "Number is palidrome");
                Console.Out.Write("Number is palidrome");
            }
            catch (Exception e)
            {
                Console.Out.Write("Number is not palidrome");

            }

            //}
        }


        [Test]
        public void VerifyHomePageIsRenderdAfterClickingOnYourLogo()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            ChormeDriver.FindElement(By.XPath("//*[@id='block_top_menu']/ul/li[2]/a")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='header_logo']/a/img")).Click();
            string URL = ChormeDriver.Url;
            Assert.AreEqual(URL, "http://automationpractice.com/index.php", "Home Page  not Launched");


        }
        [Test]
        public void VerifyContactUsPage()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            ChormeDriver.FindElement(By.XPath("//*[@id='contact-link']/a")).Click();
            var ContactUs_Text = ChormeDriver.FindElement(By.XPath("//*[@id='center_column']/h1")).Text;
            Assert.AreEqual(ContactUs_Text, "CUSTOMER SERVICE - CONTACT US", "Contact Us Page not renderd");

        }
        [TestCase("Assignment6", Category = "Type.Regression,CP.ECi,ECi.Checkout,ECi.Checkout.Attention")]
        [Test]
        public void VerifyNoResultForSerachKeyword()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            ChormeDriver.FindElement(By.XPath("//*[@id='search_query_top']")).SendKeys("ABCDEFGH");
            ChormeDriver.FindElement(By.XPath("//*[@id='searchbox']/button")).Click();
            String Search_Result = ChormeDriver.FindElement(By.XPath("//*[@id='center_column']/p")).Text;
            Assert.AreEqual(Search_Result, "No results were found for your search \"ABCDEFGH\"", " Result found");

        }
    }
}