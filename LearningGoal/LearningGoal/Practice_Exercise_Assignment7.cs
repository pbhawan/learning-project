﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using NUnit.Framework;

//using LearningProject.Automation.Utilities;

namespace LearningGoal
{
    [TestFixture]
    public class Practice_Exercise_Assignment7
    {
        [Test]
        public void CalculateValetParking()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://adam.goucher.ca/parkcalc/");
            ChormeDriver.FindElement(By.Id("Lot")).FindElement(By.XPath(".//option[contains(text(),'Valet Parking')]")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='EntryTime']")).Clear();
            ChormeDriver.FindElement(By.XPath("//*[@id='EntryTime']")).SendKeys("2:00");
            ChormeDriver.FindElement(By.XPath("/html/body/form/table/tbody/tr[2]/td[2]/font/input[3]")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='EntryDate']")).Clear();
            ChormeDriver.FindElement(By.XPath("//*[@id='EntryDate']")).SendKeys("01/19/2018");
            ChormeDriver.FindElement(By.XPath("//*[@id='ExitTime']")).Clear();
            ChormeDriver.FindElement(By.XPath("//*[@id='ExitTime']")).SendKeys("3:00");           
            ChormeDriver.FindElement(By.XPath("/html/body/form/table/tbody/tr[3]/td[2]/font/input[3]")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='ExitDate']")).Clear();
            ChormeDriver.FindElement(By.XPath("//*[@id='ExitDate']")).SendKeys("01/20/2018");
            ChormeDriver.FindElement(By.XPath("/html/body/form/input[2]")).Click();
            string Cost = ChormeDriver.FindElement(By.XPath("/html/body/form/table/tbody/tr[4]/td[2]/span/font/b")).Text;
           // Assert.Greater()
            
        }
        [Test]
        public void VerifyLessThen10ProductDisplayOnPage()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            ReadOnlyCollection<IWebElement> Products = ChormeDriver.FindElements(By.XPath("//*[@id='homefeatured']/li"));
           
        }

        [Test]
        public void Verify3ProductsonMenuDressesSummerDresses()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            ChormeDriver.FindElement(By.XPath("//*[@id='block_top_menu']/ul/li[2]/a")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='categories_block_left']/div/ul/li[3]/a")).Click();
            ReadOnlyCollection<IWebElement> Products = ChormeDriver.FindElements(By.XPath("//*[@id='center_column']/ul/li"));
        }


        [Test]
        public void FilterProductofColorWhiteonMenuSummerDresses()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            ChormeDriver.FindElement(By.XPath("//*[@id='block_top_menu']/ul/li[2]/a")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='categories_block_left']/div/ul/li[3]/a")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='layered_id_attribute_group_8']")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='layered_id_attribute_group_8']")).Click();
            //ChormeDriver.FindElement(By.XPath("//*[@id='layered_price_slider']/a[1]")).
        }
        [Test]
        public void FilterProductWithRangeSlider()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            ChormeDriver.FindElement(By.XPath("//*[@id='block_top_menu']/ul/li[2]/a")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='categories_block_left']/div/ul/li[3]/a")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='layered_id_attribute_group_8']")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='layered_id_attribute_group_8']")).Click();
            //ChormeDriver.FindElement(By.XPath("//*[@id='layered_price_slider']/a[1]")).
        }
        [Test]
        public void AddItemtoWishlist()
        {
            IWebDriver ChormeDriver = new ChromeDriver(@"C:\Workspace\Learning\LearningGoal\LearningGoal\Browser");
            ChormeDriver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            ChormeDriver.FindElement(By.XPath("//*[@id='block_top_menu']/ul/li[2]/a")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='categories_block_left']/div/ul/li[3]/a")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='center_column']/ul/li[1]/div/div[1]/div/a[1]/img")).Click();
            ChormeDriver.FindElement(By.XPath("//*[@id='add_to_cart']/button/span")).Click();
            //ChormeDriver.FindElement(By.XPath("//*[@id='layered_price_slider']/a[1]")).
        }
    }
}
