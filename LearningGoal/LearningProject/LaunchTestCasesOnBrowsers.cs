﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LearningProject.Automation.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;



namespace LearningProject.Automation.LaunchTestCasesOnBrowsers
{
    [TestClass]
    public class LaunchTestCasesOnBrowsers
    {      
       [TestMethod]
        public void LaunchTestCaseOnChrome()
        {
            IWebDriver driver = new ChromeDriver(@"C:\Main\ECI.Testing.UI\QAAutomationMVC\Browser");
            driver.Navigate().GoToUrl("https://www.facebook.com/login/");
        }
        [TestMethod]
        public void LaunchTestCaseOnFirefox()
        {
            FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Main\ECI.Testing.UI\QAAutomationMVC\Browser");
            service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
            IWebDriver driver = new FirefoxDriver(service);
            driver.Navigate().GoToUrl("https://bing.com");
        }
        [TestMethod]
        public void LaunchTestCaseOnIE()
        {
            IWebDriver driver = new InternetExplorerDriver();
            driver.get
        }
        [TestMethod]
        public void LaunchTestCaseOnSafari()
        {
        }
    }
}
