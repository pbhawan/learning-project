﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.IO;

namespace LearningProject.Automation.Utilities
{
    public static class BrowserFactory
    {
        private static IWebDriver browser;

        public static string OutputFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FailedTests");

        private static readonly string ApplicationPathBaseDirectory = string.Format(@"{0}..\..\..\", AppDomain.CurrentDomain.BaseDirectory);

        private static readonly string BrowserPath = string.Format(@"{0}Browser", ApplicationPathBaseDirectory);

        public static IWebDriver InitBrowser(Browsers selectedBrowser)
        {

            if (browser == null)
            {
                switch (selectedBrowser)
                {
                    case Browsers.Firefox:
                        browser = StartFirefox();
                        break;

                    case Browsers.RemoteFireFox:
                        browser = StartRemoteFirefox();
                        break;

                    case Browsers.RemoteChrome:
                        browser = StartRemoteChrome();
                        break;

                    case Browsers.InternetExplorer:
                        browser = StartInternetExplorer();
                        break;

                    case Browsers.Chrome:
                        browser = StartChrome();
                        break;

                }
            }
            //    browser.Manage().Window.Maximize();
            return browser;
        }

        public static IWebDriver LaunchBrowser(Browsers browserType)
        {
            if (browser == null)
                InitBrowser(browserType);
            return browser;
        }

        public static void TerminateBrowser()
        {
            browser = null;
        }

        private static InternetExplorerDriver StartInternetExplorer()
        {
            var internetExplorerOptions = new InternetExplorerOptions
            {
                IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                InitialBrowserUrl = "about:blank",
                EnableNativeEvents = true
            };
            return new InternetExplorerDriver(Directory.GetCurrentDirectory(), internetExplorerOptions);
        }

        private static FirefoxDriver StartFirefox()
        {
            string gecodriverpath = string.Format(@"{0}Browser", ApplicationPathBaseDirectory);
            Environment.SetEnvironmentVariable("webdriver.geco.driver", BrowserPath);
            FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(BrowserPath, "geckodriver.exe");
            service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
            service.HideCommandPromptWindow = true;
            service.SuppressInitialDiagnosticInformation = true;
            FirefoxProfile firefoxProfile = new FirefoxProfile();
            firefoxProfile.SetPreference("reader.parse-on-load.enabled", false);
            FirefoxOptions firefoxoptions = new FirefoxOptions();
            firefoxoptions.Profile = firefoxProfile;
            var _driver = new FirefoxDriver(service, firefoxoptions, TimeSpan.FromSeconds(60));
            return _driver;
        }


        private static RemoteWebDriver StartRemoteFirefox()
        {
            const string USERNAME = "amitagarwal5";
            const string AUTOMATE_KEY = "BJLW1sTtovDCbSZwpS3P";
            //const string URL = "http://hub.browserstack.com/wd/hub";
            const string remote = "http://d-1tmqtr1-in:4444/wd/hub";

            var profile = CreateFirefoxProfile();
            DesiredCapabilities capabilities = DesiredCapabilities.Firefox();
            //capabilities.SetCapability(FirefoxDriver.ProfileCapabilityName, profile);
            //caps.SetCapability("browser", "Firefox");
            //caps.SetCapability("browser_version", "44.0");
            //caps.SetCapability("os", "Windows");
            //caps.SetCapability("os_version", "7");
            //caps.SetCapability("resolution", "1024x768");
            //caps.SetCapability("browserstack.user", USERNAME);
            //caps.SetCapability("browserstack.key", AUTOMATE_KEY);
            RemoteWebDriver driver = new RemoteWebDriver(new Uri(remote), capabilities);
            return driver;
        }

        private static RemoteWebDriver StartRemoteChrome()
        {
            const string remote = "http://d-1tmqtr1-in:4444/wd/hub";
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            RemoteWebDriver driver = new RemoteWebDriver(new Uri(remote), options.ToCapabilities());
            return driver;
        }


        private static RemoteWebDriver StartHtmlUnit()
        {
            DesiredCapabilities caps = DesiredCapabilities.HtmlUnitWithJavaScript();
            RemoteWebDriver driver = new RemoteWebDriver(caps);
            return driver;
        }

        private static ChromeDriver StartChrome()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--start-maximized");
            chromeOptions.AddArgument("--disable-web-security");
            chromeOptions.AddArgument("--no-proxy-server");
            //    chromeOptions.AddAdditionalCapability("password_manager_enabled", false);
            chromeOptions.AddUserProfilePreference("credentials_enable_service", false);
            Environment.SetEnvironmentVariable("webdriver.chrome.driver", BrowserPath);
            DesiredCapabilities capabilities = DesiredCapabilities.Chrome();
            capabilities.SetCapability("chrome.switches", "--start-maximized");
            capabilities.SetCapability("chrome.switches", "--disable-extensions");
            return new ChromeDriver(BrowserPath, chromeOptions);
        }
        private static FirefoxProfile CreateFirefoxProfile()
        {
            FirefoxProfile profile = new FirefoxProfile();
            profile.AddExtension("Tools/modify_headers-0.7.1.1-fx.xpi");
            profile.SetPreference("general.useragent.override", "UA-STRING");
            profile.SetPreference("extensions.modify_headers.currentVersion", "0.7.1.1-signed");
            profile.SetPreference("modifyheaders.headers.count", 1);
            profile.SetPreference("modifyheaders.headers.action0", "Add");
            profile.SetPreference("modifyheaders.headers.name0", "SampleHeader");
            profile.SetPreference("modifyheaders.headers.value0", "test1234");
            profile.SetPreference("modifyheaders.headers.enabled0", true);
            profile.SetPreference("modifyheaders.config.active", true);
            profile.SetPreference("modifyheaders.config.alwaysOn", true);
            profile.SetPreference("modifyheaders.config.start", true);

            return profile;
        }


    }
}